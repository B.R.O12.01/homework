// Our Services

const tab = document.querySelectorAll('.iteam-list');
const text = document.querySelectorAll('.inform-servis');

tab.forEach(element =>{
    element.addEventListener('click', () =>{
        let currentBtn = element;
        let tabId = currentBtn.getAttribute('data-text');
        let currentTab = document.querySelector(tabId)
        currentBtn.getAttribute('data-text');
        tab.forEach(element => {
            element.classList.remove('iteam-list-active');  
         })
         text.forEach(element => {
            element.classList.remove('inform-servis-active'); 
         })
            currentBtn.classList.add('iteam-list-active');  
            currentTab.classList.add('inform-servis-active')
        })
});




// Our Amazing Work


const tabFoto = document.querySelectorAll('.ofer-list-btn');
const fotoActive = document.querySelectorAll('.list');

for(let btnTab of tabFoto){
    btnTab.addEventListener('click', event => {
        tabFoto.forEach(element => {
            element.classList.remove('active')
        })
        event.target.classList.add('active');
        for(let foto of fotoActive){
            foto.classList.remove('active') 
            if(event.target.dataset.tab === foto.dataset.fotoAll){
                foto.classList.add('active')
            }else if(event.target.dataset.tab === foto.dataset.fotoOne){
                foto.classList.add('active')
                }else if(event.target.dataset.tab === foto.dataset.fotoTwo){
                    foto.classList.add('active')
                }else if(event.target.dataset.tab === foto.dataset.fotoThree){
                    foto.classList.add('active')
                } else if(event.target.dataset.tab === foto.dataset.fotoFour){
                    foto.classList.add('active')
                }
            }
    })
}



// Our Amazing Work


const addBtn = document.querySelector('.btn-add');
const addFoto = document.querySelectorAll('.list-add');
const load = document.querySelector('.lds-roller');

addBtn.addEventListener('click', () => {
    load.classList.add('active');
    addBtn.style.display = 'none';
    setInterval(() =>{
        addFoto.forEach(element => {
            element.classList.add('active')
        })
        load.classList.remove('active')
    },3000)
})







