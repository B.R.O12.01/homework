'use strike'

const tabs = document.querySelectorAll('.tabs-title');
const text = document.querySelectorAll('.content-text');

for(let singleTab of tabs){
    singleTab.addEventListener('click', (event) =>{
        document.querySelector('.tabs-title.active')?.classList.remove('active');
        document.querySelector('.content-text.active')?.classList.remove('active');
        event.target.classList.add('active');
        for(let singleText of text){
            if(singleText.dataset.contentt === event.target.dataset.text){
                singleText.classList.add('active');
            }
        }
    })
};




